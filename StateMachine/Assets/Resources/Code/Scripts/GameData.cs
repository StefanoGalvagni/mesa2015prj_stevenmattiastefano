﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Assets.Resources.Code.Interfaces;


namespace Assets.Resources.Code.Scripts
{
    public class GameData : MonoBehaviour, IGameData
    {
        // Texture used in the background scene
        public Texture2D beginStateSplash;
        public Texture2D setupStateSplash;
        public Texture2D instructionStateSplash;
        public Texture2D playStateSplash;
        public Texture2D wonStateSplash;
        public Texture2D lostStateSplash;

        // Texture for the force bar
        public Texture2D greenBar;
        public Texture2D yellowBar;
        public Texture2D redBar;
        public Texture2D backgroundBar;
        
        // class to manager Force input
        public ForceManager forceManager;

        /// <summary>
        /// Use this for initialization
        /// </summary>
        /// <returns></returns>
        void Start()
        {
            //TO DO: when scene start set default value of player lives
            forceManager = new ForceManager();
        }

        /// <summary>
        /// Update is called once per frame
        /// </summary>
        /// <returns></returns>
        void Update()
        {
        }

        /// <summary>
        /// Reset game parameter to initial value
        /// </summary>
        /// <returns></returns>
        public void ResetGameData()
        {
            //TO DO: when scene Restart set default value of player lives
            forceManager.ResetForce();
        }

    }
}